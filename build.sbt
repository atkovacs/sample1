lazy val root = (project in file(".")).
  settings(
    name := "socinfo",
    version := "0.7",
    scalaVersion := "2.11.7",
    scalacOptions ++= Seq("-deprecation", "-feature"),
    libraryDependencies += "com.github.nscala-time" % "nscala-time_2.10" % "2.10.0",
    libraryDependencies += "com.github.scala-incubator.io" % "scala-io-file_2.11" % "0.4.3-1",
    libraryDependencies += "org.scalaj" % "scalaj-http_2.11" % "2.2.1",
    libraryDependencies += "org.ccil.cowan.tagsoup" % "tagsoup" % "1.2.1",
    libraryDependencies += "org.scala-lang.modules" % "scala-xml_2.11" % "1.0.5"
  )
