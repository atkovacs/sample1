import socinfo.Io._

import com.github.nscala_time.time.Imports._
import scalax.file.{FileOps, Path}

package socinfo {

  package object Scraper {

    def main(args: Array[String]): Unit = {

      badTranslationFile.write("")

      if (DateTime.now.toString contains "01T10:0") {
        logFile.write("")
      }

      logFile.append("\n================================\n"
        + DateTime.now
        + "\n================================\n")

      if (args.length != 1) {
        myPrint("Bad number of arguments")
      } else start(args(0))

      def start(command: String): Unit = command match {
        case "get-free" => {
          processContent(false, "free")
        }
        case "get-premium" => {
          processContent(false, "premium")
        }
        case "get-all" => {
          processContent(false, "free")
          processContent(false, "premium")
        }
        case "send-free" => {
          processContent(true, "free")
        }
        case "send-premium" => {
          processContent(true, "premium")
        }
        case "send-all" => {
          processContent(true, "free")
          processContent(true, "premium")
        }
        case "update-h" => {
          val premiumPosts: Seq[PagePost] = premiumMainPage.parseSubPages(premiumMainPage.subPages)

          val postToUpdate = premiumPosts filter (_.
            get("title").
            head.
            head.
            toString
            equals "F")

          if (postToUpdate.nonEmpty) {
            targetPage.updatePost(postToUpdate.head)
          } else myPrint("There was no page to update")
        }
        case _ => {
          println("Bad argument")
        }
      }

      def processContent(send: Boolean, accessLevel: String) = {

        myPrint("processing: " + accessLevel)

        val lastTitleFile = if (accessLevel == "free") {
          Path("last_free.title")
        } else Path("last_premium.title")

        val titleLines: List[String] = lastTitleFile.string.split('\n').toList
        if (titleLines.length < 15) {
          lastTitleFile.append("") //do nothing
        } else updateFile

        def updateFile = {
          def appendToFile(string: String) = {
            lastTitleFile.append("\n" + string)
          }

          lastTitleFile.write("")
          (titleLines.takeRight(15)) map appendToFile
          val newString = lastTitleFile.string.tail
          lastTitleFile.write(newString + "\n")
        }

        val posts: Seq[PagePost] = if (accessLevel == "free") {
          freeMainPage.parseSubPages(freeMainPage.subPages)
        } else premiumMainPage.parseSubPages(premiumMainPage.subPages)

        if (send) {
          targetPage.sendPosts(posts) //newest goes first
        }

        val postToUpdate = accessLevel match {
          case "premium" => posts filter
            (_.get("title").
              head.
              head.
              toString
              equals "F")
          case "free" => posts filter
            (_.get("title").
              head.
              head.
              toString
              equals "H")
          case _ => Seq.empty
          }

          if (postToUpdate.nonEmpty) {
            targetPage.updatePost(postToUpdate.head)
          } else myPrint("there was no page to update")

        logFile.append(logsString)
      }
    }
  }
}
