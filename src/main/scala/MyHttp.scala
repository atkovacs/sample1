import scalaj.http.BaseHttp

package socinfo {

  object MyHttp extends BaseHttp(
    userAgent = "Mozilla/5.0 (compatible; " +
    "MSIE 9.0; " +
    "Windows NT 6.1; " +
    "Win64; " +
    "x64; " +
      "Trident/5.0)"

    // userAgent = "Mozilla/5.0 (compatible; " +
    //   "MSIE 10.0; " +
    //   "Windows NT 6.1; " +
    //   "WOW64; " +
    //   "Trident/6.0)"
   )
}
