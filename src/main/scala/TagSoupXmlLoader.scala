import scala.xml.{Elem, Node, Text, XML}
import scala.xml.factory.XMLLoader
import org.ccil.cowan.tagsoup.jaxp.SAXFactoryImpl

package socinfo {

  object TagSoupXmlLoader {
    private val factory = new SAXFactoryImpl()

    def get(): XMLLoader[Elem] = {
      XML.withSAXParser(factory.newSAXParser())
    }
  }

}
