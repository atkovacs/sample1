import socinfo.Io._

import scala.language.postfixOps
import scala.xml.{Elem, Node, Text, XML}
import scalax.file.{FileOps, Path}

package socinfo {

  object SourcePage {}

  abstract class SourcePage {

    implicit class BodyTranslations(body: String) {

      def translateThis = {
        val eng_str = "eng_str"
        val hun_str = "hun_str"
        if (body contains eng_str.take(30)) {
          body.replace(eng_str,hun_str)
        } else body
      }

      def translateThat = {
        if (body contains "str") {
          val eng_str =  "eng_str"

          val hun_str = "hun_str"
          body.replace(eng_str, hun_str)
        } else body
      }

    }

    def translateBody(body: String, pairs: List[(String, String)]) = {
      val filteredBody = body.
        translateThis.
        translateThat

      translateWords(filteredBody, pairs)
    }

    def translateWords(body: String, pairs: List[(String, String)]): String = {
      if (pairs.nonEmpty) {
        val newBody = if (body contains ((pairs.head)_1)) {
          body.replace((pairs.head)_1, (pairs.head)_2)
        } else body
        val newPairs = pairs.drop(1)
        translateWords(newBody, newPairs)
      } else body
    }

    val url: String

    def login(): (String, String)

    val (cookieName, cookieValue) = this.login

    val subPages: Seq[Node] = this.getSubPages(this.url)

    def getSubPages(url: String): Seq[Node] = {

      val response = MyHttp(url).
        cookie(cookieName, cookieValue).
        asString

      val fullPage: Elem = TagSoupXmlLoader.
        get.
        loadString(response.body)

      val postDivNodes: Seq[Node] = (fullPage \\
        "div" filter (_ \
          "@class" contains Text("post-title-index")))

      postDivNodes flatMap (_ \\ "a" \ "@href")
    }

    def parseSubPages(subPages: Seq[Node]): Seq[PagePost] = {
      subPages map parseOnePage
    }

    def translateText(text: String, files: List[FileOps]): String = {

      def translateFromOneFile(file: FileOps): List[String] = {

        def splitLine(line: String): (String, String) = {
          try {
            val english = line.takeWhile(_ != '%')
            (english, line.diff(english).tail)
          } catch {
            case _: Throwable => {
              badTranslationFile.append(line + "\n")
              ("", "")
            }
          }
        }

        def checkTag(eng: String, hun: String): String = {
          if (text equals eng) {
            hun
          } else ""
        }

        val lines: List[String] = file.
          string.
          split('\n').
          toList

        val pairs: List[(String, String)] = lines map splitLine

        (pairs map (pair => (checkTag _).tupled(pair)) filter (_.nonEmpty))
      }

      val res: List[String] = (files flatMap translateFromOneFile) filter (_.nonEmpty)

      if (res.length > 0) {
        res.head
      } else ""
    }

    def translateTag(tag: String) = {
      translateText(
        tag,
        List(Path("c1_translate.con"), Path("c2_translate.con"))
      )
    }

    def findTags(iter: Iterator[Node], list: List[String]): List[String] = {

      val tagList = if (iter.hasNext) {
        val newList = iter.next.text :: list
        findTags(iter, newList)
      } else list

      tagList
    }

    def findAccessType(cookieName: String, title: String) = {
      if (cookieName.length > 0) {
        List("premium")
      } else if (title == "H") {
        List("S")
      } else List("free")
    }

    def splitLine(line: String): (String, String) = {
      try {
        val english = line.takeWhile(_ != '%')
        (english, line.diff(english).tail)
      } catch {
        case _: Throwable => {
          badTranslationFile.append(line + "\n")
          ("", "")
        }
      }
    }

    def findTitle(tags: List[String], currentUrl: String) = {

      def translateCompetition(competition: String): String = {
        translateText(competition, List(Path("c2_translate.con")))
      }

      def translateCountry(country: String): String = {
        translateText(country, List(Path("c1_translate.con")))
      }

      val competitionList = tags map translateCompetition filter (_ != "")
      val countryList = tags map translateCountry filter (_ != "")

      val competition = if (competitionList.length > 0) {
        competitionList.head
      } else "S"

      val country = if (countryList.length > 0) {
        countryList.head
      } else "L"

      if ((currentUrl contains "one")
        || (currentUrl contains "two")
        || (currentUrl contains "three")
        || (currentUrl contains "four")) {
        List("not_used")
      } else if (currentUrl contains "y") {
        List("H")
      } else if ((countryList.length > 0) && (countryList.head equals "F")) {
        List(country)
      } else List(competition + " (" + country + ")")
    }

    def findBody(page: Elem) = {

      val translateFile = Path("t_translate.con")

      val lines: List[String] = translateFile.
        string.
        split('\n').
        toList

      val pairs: List[(String, String)] = lines map splitLine

      val textDivNode: Node = (page \\
        "div" filter (_ \
          "@class" contains Text("post-content-page"))).head

      val pNodes = (textDivNode \\ "p").dropRight(1)

      List(translateBody(pNodes.toString, pairs))
    }

    def parseOnePage(page: Node): PagePost = {
      val currentUrl = page.toString

      myPrint("parsing: " + currentUrl)

      val response = MyHttp(currentUrl).
        cookie(cookieName, cookieValue).
        asString

      val fullPage: Elem = TagSoupXmlLoader.
        get.
        loadString(response.body)

      val tagNodes: Iterator[Node] = (fullPage \\
        "a" filter (_ \
          "@rel" contains Text("tag"))).iterator

      val body = findBody(fullPage)
      val tags = findTags(tagNodes, List())
      val translatedTags = tags map translateTag
      val title = findTitle(tags, currentUrl)
      val accessType = findAccessType(cookieName, title.head)

      Map(
        "title" -> title,
        "body" -> body,
        "tags" -> translatedTags,
        "accessType" -> accessType
      )
    }
  }
}
