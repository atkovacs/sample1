import scalax.file.{FileOps, Path}

package socinfo {

  object Io {

    val logFile = Path("log_with_dates.log")
    val tempLogFile = Path("temp.log")
    val badTranslationFile = Path("translate_problems.log")

    var logsString = ""

    def myPrint(string: String) = {
      println(string)
      logsString = logsString + string + "\n"
    }
  }
}
