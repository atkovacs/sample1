import socinfo.Io._

import scala.xml.{Elem, Node, Text, XML}
import scala.xml.factory.XMLLoader
import scalax.file.{FileOps, Path}

package socinfo {
  object targetPage {
    def mainUrl = "http://myurl"

    def login(): (String, String) = {
      val targetPagePostResponse = MyHttp(mainUrl).
        postForm(Seq(
          "name" -> "name",
          "pass" -> "pass",
          "form_id" -> "user_login_block",
          "op" -> "Login"
        ))
        .asString

      val cookie = targetPagePostResponse.
        cookies.
        head.
        toString

      val cookieName = cookie.takeWhile(_ != '=')
      (cookieName, cookie.diff(cookieName).tail)
    }

    def formatTags(tags: Iterator[String], result: String): String = {
      if (tags.hasNext) {
        val newResult = tags.
          next.
          toString +
          ", " +
          result

        formatTags(tags, newResult)
      } else result
    }

    def sendPosts(posts: Seq[PagePost]): Unit = {

      val accessType = posts.
        head.
        get("accessType").
        head.
        head.
        toString

      val lastTitleFile = if (accessType == "free") {
        Path("last_free.title")
      } else Path("last_premium.title")

      val uploadedTitles = lastTitleFile.string

      def recordPost(post: PagePost) = {
        val title = post.
          get("title").
          head.
          head.
          toString

        val body = post.
          get("body").
          head.
          head.
          toString

        if ((title != "not_used") && !(lastTitleFile.string contains title)) {
          lastTitleFile.append(title + "\n")
        }
      }

      def titleMatch(post: PagePost): Boolean = {
        val title = post.
          get("title").
          head.
          head.
          toString

        uploadedTitles contains title
      }

      posts.reverse map recordPost

      val postsToSend = posts filterNot titleMatch
      postsToSend map sendOnePost
    }

    def findUrl(accessType: String) = accessType match {
      case "premium" => "url1"
      case "free" => "url2"
      case "statisztika" => "url3"
      case _ => ""
    }

    def findFormID(accessType: String) = accessType match {
      case "premium" => "id1"
      case "free" => "id2"
      case "statisztika" => "id3"
      case _ => ""
    }

    def updatePost(post: PagePost): Unit = {
      this.deletePost(post)
      this.sendOnePost(post)
    }

    def deletePost(post: PagePost): Unit = {

      myPrint("deleting: " + post.get("title").head.head.toString)

      def findNodeNumber(node: Node) = {

        if (node.
          toString contains
          post.get("title").head.head.toString) {
          node.toString.takeWhile(_ != '>').
            dropRight(1).
            reverse.
            takeWhile(_ != '-').
            reverse.
            toInt
        } else 0
      }

      val contentUrl = "http://myurl/content"
      val (cookieName, cookieValue) = this.login

      val contentResponse = MyHttp(contentUrl).
        cookie(cookieName, cookieValue).
        asString

      val loader: XMLLoader[Elem] = TagSoupXmlLoader.get
      val fullPage: Elem = loader.loadString(contentResponse.body)

      val infoNodes = (fullPage \\
        "label" filter (_ \
          "@class" contains Text("element-invisible")))

      val mappedNodes = ((infoNodes map findNodeNumber).
        filter(_ > 0))

      val nodeToDelete = if (mappedNodes.length > 0) {
        mappedNodes.head
      } else "noNode"

      val deleteUrl = "http://myurl/node/" +
        nodeToDelete.toString +
        "/delete/"

      val preliminaryResponse = MyHttp(deleteUrl).
        cookie(cookieName, cookieValue).
        asString

      val fullDelPage: Elem = loader.loadString(preliminaryResponse.body)

      val formTokenNodes = (fullDelPage \\
        "input" filter (_ \
          "@name" contains Text("form_token")))

      val formTokenNode = if (formTokenNodes.length > 0) {
        formTokenNodes.head
      } else formTokenNodes

      val form_token: String = if (formTokenNodes.length > 0) {
        (formTokenNode \ "@value").toString
      } else "noToken"

      val deleteResponse = MyHttp(deleteUrl).
        cookie(cookieName, cookieValue).
        postForm(Seq(
          "confirm" -> "1",
          "form_id" -> "node_delete_confirm",
          "form_token" -> form_token,
          "nodes[" + nodeToDelete.toString + "]" -> nodeToDelete.toString,
          "op" -> "Delete",
          "operation" -> "delete"
        ))
        .asString

      myPrint("deleted")
    }

    def sendOnePost(post: PagePost): Unit = {

      val (cookieName, cookieValue) = this.login

      val title = post.
        get("title").
        head.
        head.
        toString

      val accessType = post.
        get("accessType").
        head.
        head.
        toString

      myPrint("uploading: " + title)

      val sendUrl = findUrl(accessType)
      val form_id = findFormID(accessType)

      val addPreliminaryResponse = MyHttp(sendUrl).
        cookie(cookieName, cookieValue).
        asString

      val loader: XMLLoader[Elem] = TagSoupXmlLoader.get
      val fullPage: Elem = loader.
        loadString(addPreliminaryResponse.body)

      val formIDNode: Node = (fullPage \\
        "input" filter (_ \
          "@name" contains Text("form_build_id"))).
          head

      val form_build_id: String = (formIDNode \ "@value").
        toString

      val formTokenNode: Node = (fullPage \\
        "input" filter (_ \
          "@name" contains Text("form_token"))).
          head

      val form_token: String = (formTokenNode \ "@value").
        toString

      val tags = formatTags(
        post.
        get("tags").
        head.
        iterator,
        ""
      )

      if (title != "not_used") {
        val postRequest = Seq(
          "menu[parent]" -> "main-menu:0",
          "menu[weight]" -> "0",
          "comment" -> "1",
          "form_build_id" -> form_build_id,
          "form_token" -> form_token,
          "path[pathauto]" -> "1",
          "promote" -> "1",
          "status" -> "1",
          "name" -> "admin",
          "field_tags[und]" -> tags,
          "form_id" -> form_id,
          "body[und][0][format]" -> "full_html",
          "body[und][0][value]" -> post.get("body").head.head.toString,
          "title" -> title,
          "op" -> "Save"
        )

      val addResponse = MyHttp(sendUrl).
        cookie(cookieName, cookieValue).
        postForm(postRequest).
        asString
      }
    }
  }
}
