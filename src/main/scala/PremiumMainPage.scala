package socinfo {

  object premiumMainPage extends SourcePage {

    lazy val url = "http://myurl/extra"

    def login(): (String, String) = {
      val response = MyHttp(url).
        postForm(Seq(
          "access_login" -> "login",
          "access_password" -> "pass",
          "Submit" -> "Login"
        ))
        .asString
      ("verify", "+ cookieString +")
    }
  }
}
